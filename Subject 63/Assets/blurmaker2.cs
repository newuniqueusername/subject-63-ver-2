﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class Blurmaker2 : MonoBehaviour
{
    public float BlurCount2;

    // Use this for initialization
    void Start()
    {

        BlurCount2 = gameObject.GetComponent<BlurOptimized>().blurSize;
        InvokeRepeating("BlurTime", 1.0f, 1.0f);

    }
    void BlurTime()
    {
        print(BlurCount2);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
