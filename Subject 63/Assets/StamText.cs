﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StamText : MonoBehaviour {
    public float StamBar;
    public Text ScoreText;
	// Use this for initialization
	void Update () {
        {
            StamBar = GameObject.Find(Tags.player).GetComponent<PlayerControl>().stamina;
            if (StamBar >= 100) 
            ScoreText.text =  ("STAMINA: llllllllllllllllllll");
            if (StamBar == 95)
                ScoreText.text = ("STAMINA: lllllllllllllllllll");
            if (StamBar == 90)
                ScoreText.text = ("STAMINA: llllllllllllllllll");
            if (StamBar == 85)
                ScoreText.text = ("STAMINA: lllllllllllllllll");
            if (StamBar == 80)
                ScoreText.text = ("STAMINA: llllllllllllllll");
            if (StamBar == 75)
                ScoreText.text = ("STAMINA: lllllllllllllll");
            if (StamBar == 70)
                ScoreText.text = ("STAMINA: llllllllllllll");
            if (StamBar == 65)
                ScoreText.text = ("STAMINA: lllllllllllll");
            if (StamBar == 60)
                ScoreText.text = ("STAMINA: llllllllllll");
            if (StamBar == 55)
                ScoreText.text = ("STAMINA: lllllllllll");
            if (StamBar == 50)
                ScoreText.text = ("STAMINA: llllllllll");
            if (StamBar == 45)
                ScoreText.text = ("STAMINA: lllllllll");
            if (StamBar == 40)
                ScoreText.text = ("STAMINA: llllllll");
            if (StamBar == 35)
                ScoreText.text = ("STAMINA: lllllll");
            if (StamBar == 30)
                ScoreText.text = ("STAMINA: llllll");
            if (StamBar == 25)
                ScoreText.text = ("STAMINA: lllll");
            if (StamBar == 20)
                ScoreText.text = ("STAMINA: llll");
            if (StamBar == 15)
                ScoreText.text = ("STAMINA: lll");
            if (StamBar == 10)
                ScoreText.text = ("STAMINA: ll");
            if (StamBar == 5)
                ScoreText.text = ("STAMINA: l");
            if (StamBar == 0)
                ScoreText.text = ("STAMINA: NONE");
            //print(StamBar);

        }
    }
}
	

