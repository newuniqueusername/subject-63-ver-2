﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GlassesText : MonoBehaviour {
    public float GlassesCount;
    public Text GlassesTxt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GlassesCount = GameObject.Find("Player").GetComponent<PlayerInventory>().eyeContacts;
        if (GlassesCount == 0)
            GlassesTxt.text = "Glasses: 0";
        if (GlassesCount == 1)
            GlassesTxt.text = "Glasses: 1";
        if (GlassesCount == 2)
            GlassesTxt.text = "Glasses: 2";
        if (GlassesCount == 3)
            GlassesTxt.text = "Glasses: 3";
        if (GlassesCount == 4)
            GlassesTxt.text = "Glasses: 4";

    }
}
