﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnergyCounter : MonoBehaviour
{
    public float EnergyCount;
    public Text EnergyTxt;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        EnergyCount = GameObject.Find("Player").GetComponent<PlayerInventory>().eneryDrink;
        if (EnergyCount == 0)
            EnergyTxt.text = "Energy drinks: 0";
        if (EnergyCount == 1)
            EnergyTxt.text = "Energy drinks: 1";
        if (EnergyCount == 2)
            EnergyTxt.text = "Energy drinks: 2";
        if (EnergyCount == 3)
            EnergyTxt.text = "Energy drinks: 3";
        if (EnergyCount == 4)
            EnergyTxt.text = "Energy drinks: 4";

    }
}
