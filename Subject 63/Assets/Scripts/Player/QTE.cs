﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTE : MonoBehaviour {

    public GameObject inputKeyDisplay;
    string text;
    public GameObject progressBox;
    public int QTEGen;
    public int WaitingForKey;
    public int CorrectKey;
    public int CountingDown;

    private void Start()
    {
        text = inputKeyDisplay.GetComponent<Text>().text; 
    }

    private void Update()
    {
        if (WaitingForKey == 0) {
            QTEGen = Random.Range(1, 6);
            CountingDown = 1;

            if (QTEGen == 1) {
                WaitingForKey = 1;
                text = "[W]";
            }

            if (QTEGen == 2)
            {
                WaitingForKey = 1;
                text = "[A]";
            }

            if (QTEGen == 3)
            {
                WaitingForKey = 1;
                text = "[S]";
            }

            if (QTEGen == 4)
            {
                WaitingForKey = 1;
                text = "[D]";
            }

            if (QTEGen == 5)
            {
                WaitingForKey = 1;
                text = "[SPACE]";
            }
        }

        /*if (QTEGen == 1) {
            if (Input.anyKeyDown) {
                if (Input.GetButtonDown("WKey"))
                {
                    CorrectKey = 1;
                    StartCoroutine(KeyPressing());
                }
                else {
                    CorrectKey = 2;
                    StartCoroutine(KeyPressing());
                }
            }
        }

        if (QTEGen == 3)
        {
            if (Input.anyKeyDown)
            {
                if (Input.GetButtonDown("AKey"))
                {
                    CorrectKey = 1;
                    StartCoroutine(KeyPressing());
                }
                else
                {
                    CorrectKey = 2;
                    StartCoroutine(KeyPressing());
                }
            }
        }

        if (QTEGen == 1)
        {
            if (Input.anyKeyDown)
            {
                if (Input.GetButtonDown("SKey"))
                {
                    CorrectKey = 1;
                    StartCoroutine(KeyPressing());
                }
                else
                {
                    CorrectKey = 2;
                    StartCoroutine(KeyPressing());
                }
            }
        }

        if (QTEGen == 1)
        {
            if (Input.anyKeyDown)
            {
                if (Input.GetButtonDown("DKey"))
                {
                    CorrectKey = 1;
                    StartCoroutine(KeyPressing());
                }
                else
                {
                    CorrectKey = 2;
                    StartCoroutine(KeyPressing());
                }
            }
        }

        if (QTEGen == 1)
        {
            if (Input.anyKeyDown)
            {
                if (Input.GetButtonDown("SpaceKey"))
                {
                    CorrectKey = 1;
                    StartCoroutine(KeyPressing());
                }
                else
                {
                    CorrectKey = 2;
                    StartCoroutine(KeyPressing());
                }
            }
        }

        IEnumerator KeyPressing () {
            QTEGen = 4;
            if (CorrectKey == 1) {
                CountingDown = 2;
                progressBox.GetComponent<Text>().text = "PASS!!!";
                yield return new WaitForSeconds(1.5f);
                CorrectKey = 0;
                progressBox.GetComponent<Text>().text = "";
                inputKeyDisplay.GetComponent<Text>().text = "";
                yield return new WaitForSeconds(1.5f);
                WaitingForKey = 0;
                CountingDown = 1;
            }
        }

        IEnumerator CountDown() {
            yield return new WaitForSeconds(3.5f);
            if (CountingDown == 1) {
                QTEGen = 4;
                CountingDown = 2;
                progressBox.GetComponent<Text>().text = "FAIL!!!!";
                yield return new WaitForSeconds(1.5f);
                CorrectKey = 0;
                progressBox.GetComponent<Text>().text = "";
                inputKeyDisplay.GetComponent<Text>().text = "";
                yield return new WaitForSeconds(1.5f);
                WaitingForKey = 0;
                CountingDown = 1;
            }
        }*/
    }
}
