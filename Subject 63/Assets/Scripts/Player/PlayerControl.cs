﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PlayerControl : MonoBehaviour
{

    public float speed;
    public float stamina;
    public float MaxStamina;
    public float MinStamina;
    public bool running;
    public float xVelocity;
    public float zVelocity;
    public float speed2;
    public float speedTimer;
    public bool speedSerum;
    public float MaxSpeed;
    public float WalkSpeed;
    public bool strengthSerum;
    public bool sightSerum;
    public float strength;
    public float sight;

    public GameObject Camera;
    public GameObject speedBox;
    private Rigidbody rb;
    private GameObject player;
    private PlayerInventory inventory;
    public GameObject Contacts;
    private BlurOptimized blur;
    public AudioClip clip;
    private AudioSource source;
    public bool movement;
    public GameObject BrokenGlasses;

    void Start()
    {
        BrokenGlasses.SetActive(false);
        rb = GetComponent<Rigidbody>();
        //InvokeRepeating("decreaseSpeed", 10.0f, 10.0f);
        InvokeRepeating("PlayerOne", 10.0f, 10.0f);
        movement = false;
        running = false;

        player = GameObject.FindGameObjectWithTag("Player");
        inventory = player.GetComponent<PlayerInventory>();
        blur = GameObject.FindGameObjectWithTag("Camera").GetComponent<BlurOptimized>();

        speedTimer = 5;
        speedSerum = false;
        strengthSerum = false;
        strength = 0;
        sight = 0;
        stamina = 100;
        MaxStamina = 100;
        MinStamina = 0;
        MaxSpeed = 30;
        WalkSpeed = 20;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == speedBox)
        {
            inventory.eneryDrink += 1;
            if (other.gameObject == Contacts)
            {
                inventory.eyeContacts += 1;
            }
        }
            /*if (speed < 20)
        {
            speed += 5;
        }
        if (speed > 20)
        {
            speed = 20;
        }*/


    }
    void PlayerOne()
    {
       // print("hi");
    }

    /*void decreaseSpeed()
    {
        if (speed > 0)
        {
            speed -= 1;
        }
        if (stamina > 5)
        {
            stamina -= 1;

        }
    }*/

            
    void Awake()
    {

        source = GetComponent<AudioSource>();

    }


    void Update()

    {
        if (stamina >= MaxStamina) {
            stamina = MaxStamina;
        }

        if (stamina <= MinStamina) {
            stamina = MinStamina;
        }

        if (Input.GetKey(KeyCode.W))
        {

            rb.velocity = transform.forward * speed;
            movement = true;

        }

        if (Input.GetKey(KeyCode.S))
        {

            rb.velocity = -transform.forward * speed;
            movement = true;
        }

        if (Input.GetKey(KeyCode.D))
        {

            rb.velocity = transform.right * speed;
            movement = true;
        }

        if (Input.GetKey(KeyCode.A))
        {

            rb.velocity = -transform.right * speed;
            movement = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = MaxSpeed;
            running = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift)) {
            speed = WalkSpeed;
            running = false;
        }
        if (stamina <= 0)
        {
            speed = WalkSpeed;
        }
        if (running == true) {
            stamina -= (1 * 0.25f/* * Time.deltaTime*/);
            
        }
        if (running == false) {
            stamina += (1  * 0.25f);
            speed = WalkSpeed;
        }

        /*if (Input.GetKeyDown(KeyCode.E) && inventory.eneryDrink > 0)
        {
            inventory.eneryDrink -= 1;
            if (speed < 20)
            {
                stamina += 5;
                speed += 5;
                    
            }
            if (speed >= 20)
            {
                speed = 20;
            }
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha1) && inventory.speedSerum > 0) {
            MaxSpeed += 10;
            inventory.serumY += 10;
            inventory.serumR -= 5;
            inventory.serumB -= 5;
            speedSerum = true;
        }

        /*if (speedSerum == true) {
            speedTimer -= (1 * Time.deltaTime);
        }

        if (speedTimer <= 0 && speedSerum == true) {
            speed = 20;
            speedSerum = false;
            speedTimer = 5;
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha2) && inventory.strengthSerum > 0) {
            strength += 10;
            inventory.serumR += 10;
            inventory.serumB -= 5;
            inventory.serumY -= 5;
            strengthSerum = true;
        }

        if (strengthSerum == true) {
            //stuff that happens when strength serum is active
        }

        if (Input.GetKeyDown(KeyCode.Alpha3) && inventory.sightSerum > 0)
        {
            sight += 10;
            inventory.serumB += 10;
            inventory.serumR -= 5;
            inventory.serumY -= 5;
            sightSerum = true;
        }



        /* if (Input.GetKeyDown(KeyCode.R) && inventory.eyeContacts > 0)
         {
            /* BrokenGlasses.SetActive(false);
             inventory.eyeContacts -= 1;
             blur.blurSize -= 0.5f;
             */
        // }

    }
}


/*  void FixedUpdate()
  {
      float moveHorizontal = Input.GetAxis("Horizontal");
      float moveVertical = Input.GetAxis("Vertical");


      Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

      rb.AddForce(movement * speed);
    
  }
}
*/
