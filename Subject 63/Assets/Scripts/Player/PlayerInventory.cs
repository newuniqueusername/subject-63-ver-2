﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
public class PlayerInventory : MonoBehaviour
{

    public bool hasKey;
    public int eneryDrink;
    public int eyeContacts;
    public int speedSerum;
    public int strengthSerum;
    public int sightSerum;

    GameObject SpSerum;
    public GameObject Glasses;
    public GameObject Glasses1;
    public GameObject Glasses2;
    public GameObject Glasses3;
    private BlurOptimized Blur;
    public GameObject BrokenGlasses;

    public float serumY;
    public float serumR;
    public float serumB;

    private void Start()
    {
        Blur = GameObject.FindGameObjectWithTag("Camera").GetComponent<BlurOptimized>();
        hasKey = false;
        eneryDrink = 0;
        eyeContacts = 0;
        speedSerum = 0;
        strengthSerum = 0;
        sightSerum = 0;

        SpSerum = GameObject.FindGameObjectWithTag(Tags.speedSerum);

        serumY = 0;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && eyeContacts > 0)
        {
            
            Blur.blurSize -= Blur.blurSize;
            BrokenGlasses.SetActive(false);
            eyeContacts -= 1;
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            BrokenGlasses.SetActive(true);
            Blur.blurSize += 10;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Glasses)
        {
            eyeContacts += 1;
        }
        if (other.gameObject == Glasses1)
        {
            eyeContacts += 1;
        }
        if (other.gameObject == Glasses2)
        {
            eyeContacts += 1;
        }
        if (other.gameObject == Glasses3)
        {
            eyeContacts += 1;
        }
    }
}
