﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDrink: MonoBehaviour {

    private GameObject player;
    private PlayerInventory inventory;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        inventory = player.GetComponent<PlayerInventory>();
       
    }
    void OnTriggerEnter(Collider other) {
        if (other.gameObject == player) {
            inventory.eneryDrink += 1;
            Destroy(gameObject);
        }
        
        
        
    }
    // Update is called once per frame
    void Update () {
		
	}
}
