﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOne : MonoBehaviour {

    GameObject player;
    public AudioClip clip;
    private AudioSource source;
    void Awake()
    {

        source = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag(Tags.player);

    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == player)
        {
            source.PlayOneShot(clip);
            print("hit");
        }
    }
}

