﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contacts : MonoBehaviour {

    GameObject player;
    PlayerInventory inventory;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        inventory = player.GetComponent<PlayerInventory>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other == player) {
            inventory.eyeContacts += 1;
            Destroy(gameObject);
        }
    }
}
