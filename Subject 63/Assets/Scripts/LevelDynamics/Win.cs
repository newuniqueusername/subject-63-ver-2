﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour {
    public GameObject WinScreen;
    public GameObject player;
    SceneFaderInOut sceneFadeInOut;
    float timer;
    public float timeToEndLevel = 2f;
    bool win = false;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        //sceneFadeInOut = GameObject.FindGameObjectWithTag(Tags.fader).GetComponent<SceneFaderInOut>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (win == true)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Time.timeScale = 1.0f;
        }
    }
        private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player) {
            timer += Time.deltaTime;
            Time.timeScale = 0;
            win = true;

            WinScreen.SetActive(true);

            
            //SceneManager.LoadScene("SampleScene");
            /*if (timer >= timeToEndLevel)
                sceneFadeInOut.EndScene();*/
        }
    }
}
