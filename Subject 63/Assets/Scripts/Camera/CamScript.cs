﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour

{

    public float speedH = 2.0f;
    public float speedV = 2.0f;
	public float rotationX = 0;
	public Rigidbody rb;
	public float x;
    private float yaw = 0.0f;
    private float pitch = 0.0f;

    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X") * Time.deltaTime;
        pitch -= speedV * Input.GetAxis("Mouse Y") * Time.deltaTime;

        transform.eulerAngles = new Vector3(pitch, yaw, 0);
		 
		/*print (pitch);
		if (pitch <= -100)
			pitch = -100;
			transform.Rotate(-100, pitch, 0);
		if (pitch >= 100)
			pitch = 100;
			transform.Rotate (100, pitch, 0);*/

		//transform.Rotate.z = 0;
    }
}