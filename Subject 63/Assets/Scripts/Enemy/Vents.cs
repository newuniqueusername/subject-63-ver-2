﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vents : MonoBehaviour {

    GameObject player;
    GameObject enemy;
    bool hasEnemy;
    float timer;
    public float enemyMove;
    public Transform[] vents;
    int numVents;
    int spawnPointIndex;
    public float breakOutTime;
    float breakOut;
    bool enemyOut;


	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag(Tags.player);
        enemy = GameObject.FindGameObjectWithTag(Tags.enemyParent);
        hasEnemy = false;
        timer = 0f;
        enemyMove = 5f;
        numVents = vents.Length;
        breakOutTime = 5f;
        breakOut = 0f;
        enemyOut = false;

        spawnPointIndex = Random.Range(0, vents.Length);
    }
	
	// Update is called once per frame
	void Update () {
        timer += (1 * Time.deltaTime);

        if (timer == enemyMove && enemyOut == false) {
            spawnPointIndex = Random.Range(0, vents.Length);
        }

        //print(breakOut); //not registering player hitting trigger box
	}

    /*void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            print("enter");
        }
    }*/

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player)
        {
            breakOut += (1 * Time.deltaTime);
            //print(breakOut);
        }

        if (other.gameObject == player /*&& hasEnemy == true*/ && breakOut >= breakOutTime)
        {
            Instantiate(enemy, vents[spawnPointIndex].position, vents[spawnPointIndex].rotation);
            hasEnemy = false;
            breakOut = 0f;
            enemyOut = true;
            //print("Run peasant");
        }
    }
}
