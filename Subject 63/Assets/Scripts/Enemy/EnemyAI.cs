﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

	public float patrolSpeed = 2f;
	public float chaseSpeed = 5f;
	public float chaseWaitTime = 3f;
	public float patrolWaitTime = 0f;
	public Transform[] patrolWayPoints;

	private EnemySight enemySight;
	public UnityEngine.AI.NavMeshAgent nav;
	private Transform player;
	private PlayerHealth playerHealth;
	private LastPlayerSighting lastPlayerSighting;
	private float chaseTimer;
	private float patrolTimer;
	private int wayPointIndex;
    private Animator anim;

	void Awake () {
		enemySight = GetComponent<EnemySight>();
		nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag(Tags.player).transform;
		playerHealth = player.GetComponent<PlayerHealth>();
		lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<LastPlayerSighting>();
        anim = GameObject.FindGameObjectWithTag(Tags.enemy).GetComponent<Animator>();
	}

	void Update () {
        /*if (enemySight.playerInSight && playerHealth.health > 0f)
            Shooting();
        else*/ if (enemySight.personalLastSighting != lastPlayerSighting.resetPosition && playerHealth.health > 0f)
        {
            Chasing();
        }

        else
        {
            Patrolling();
        }
	}

	void Shooting () {
		nav.isStopped = true;
	}

	void Chasing () {
		Vector3 sightingDeltaPos = enemySight.personalLastSighting - transform.position;
        
        if (sightingDeltaPos.sqrMagnitude > 4f)
            nav.destination = enemySight.personalLastSighting;
        nav.speed = chaseSpeed;
		Vector3 lookAt = player.transform.position;
		lookAt.y = transform.position.y;
		transform.LookAt(lookAt);
        anim.SetBool("isChasing", true);
        if (nav.remainingDistance < nav.stoppingDistance)
        {
            chaseTimer += Time.deltaTime;

            if (chaseTimer >= chaseWaitTime)
            {
                lastPlayerSighting.position = lastPlayerSighting.resetPosition;
                enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
                chaseTimer = 0f;
            }
        }
        else
            chaseTimer = 0f;
    }

	void Patrolling () {
		nav.speed = patrolSpeed;
		Vector3 lookAt = nav.destination;
		lookAt.y = transform.position.y;
		transform.LookAt(lookAt);
        anim.SetBool("isChasing", false);
		if (nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance) {
			patrolTimer += Time.deltaTime;

			if (patrolTimer >= patrolWaitTime) {
				if(wayPointIndex == patrolWayPoints.Length - 1)
					wayPointIndex = 0;
				else
					wayPointIndex++;

				patrolTimer = 0;
			}
		} else 
			patrolTimer = 0;
		nav.destination = patrolWayPoints[wayPointIndex].position;
	}
}
