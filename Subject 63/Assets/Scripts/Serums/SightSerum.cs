﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightSerum : MonoBehaviour {

    GameObject player;
    PlayerControl playerMovement;
    PlayerInventory inventory;

    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerMovement = player.GetComponent<PlayerControl>();
        inventory = player.GetComponent<PlayerInventory>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player && Input.GetKey(KeyCode.E))
        {
            Destroy(gameObject);
            inventory.sightSerum += 1;
        }
    }
}
