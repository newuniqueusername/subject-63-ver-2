﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrengthDoors : MonoBehaviour {

    int DC = 10;

    GameObject player;
    PlayerControl playerControl;

    // Use this for initialization
    void Start () {
        //DC = Random.Range(10, 40);
        player = GameObject.FindGameObjectWithTag(Tags.player);
        playerControl = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<PlayerControl>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player && Input.GetKey(KeyCode.E) && playerControl.strength >= DC) {
            Destroy(gameObject);
        }
    }
}
